import React, { Component } from "react";
import ActionComponent from "./ActionComponent";

class ItemInvestigationPopup extends Component {
  renderActionDescription() {
    if (false) {
      return <p className="text-center text-danger">I can't pick this up!</p>;
    }
  }

  render() {
    const { item } = this.props;

    const { popupId } = this.props;

    console.log(item);

    return (
      <div
        className="modal fade"
        id={popupId}
        tabIndex="-1"
        role="dialog"
        aria-labelledby="itemInvestigationPopupTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="itemInvestigationPopupTitle">
                {item.name}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <p>
                {item.description +
                  (item.isInvestigated ? " " + item.descriptionAddendum : "")}
              </p>
              {this.renderActionDescription()}
            </div>
            <div className="modal-footer">
              <ActionComponent />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ItemInvestigationPopup;
