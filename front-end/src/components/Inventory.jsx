import React, { Component } from "react";

class Inventory extends Component {
  state = {
    isLoading: true
  };

  componentDidMount() {
    this.setState({ isLoading: true });

    fetch("http://localhost:64618/api/inventory/1")
      .then(response => response.json())
      .then(data => this.setState({ data, isLoading: false }));
  }

  render() {
    if (this.state.isLoading) {
      return <p>loading</p>;
    }

    const { data } = this.state;

    return (
      <React.Fragment>
        <h2>Inventory</h2>
        <div>
          <ul>
            {data.itemList.map(item => (
              <li key={item.id}>{item.name}</li>
              // <button className="btn btn-light" key={item.id}>
              //   {item.name}
              // </button>
            ))}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default Inventory;
