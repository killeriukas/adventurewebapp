import React, { Component } from "react";
import Item from "./Item";
import ItemInvestigationPopup from "./ItemInvestigationPopup";

class Room extends Component {
  state = {
    //  isLoading: true,
    //  location: "Mansion",
    name: "Kitchen",
    description:
      "Here is a {book} and in it {key} is hidden. {door} are very weak.",
    items: {
      key: {
        id: "key",
        name: "Key",
        description: "Rusty key. Looks familiar."
      },
      book: {
        id: "book",
        name: "Book",
        description: "Book with half of the pages torn out."
      },
      door: {
        id: "door",
        name: "Wide Door",
        description: "Could fit an elephant through these!"
      }
    }
  };

  // componentDidMount() {
  //   this.setState({ isLoading: true });

  //   fetch("http://localhost:64618/api/room/1")
  //     .then(response => response.json())
  //     .then(data => this.setState({ data, isLoading: false }));
  // }

  getTokenCharacter(isTokenOpen) {
    return isTokenOpen ? "}" : "{";
  }

  generateDescriptionKeysArray(description) {
    const fullDescriptionKeysArray = [];

    const descriptionLength = description.length;
    let currentIndex = 0;
    let isTokenOpen = false;
    while (currentIndex < descriptionLength) {
      let tokenIndex = description.indexOf(
        this.getTokenCharacter(isTokenOpen),
        currentIndex
      );

      if (tokenIndex === -1) {
        tokenIndex = descriptionLength;
      }

      const substringLength = tokenIndex - currentIndex;
      const slice = description.substr(currentIndex, substringLength);
      const partDescription = {
        desc: slice,
        isObject: isTokenOpen
      };
      fullDescriptionKeysArray.push(partDescription);
      currentIndex = tokenIndex + 1;
      isTokenOpen = !isTokenOpen;
    }

    return fullDescriptionKeysArray;
  }

  createPopupId(key) {
    return "itemInvestigationPopup_" + key;
  }

  conditionalLink(isObject, key, items) {
    if (isObject) {
      const item = items.find(x => x.itemTemplate.id == key);
      const template = item.itemTemplate;
      return (
        <Item
          key={key}
          itemName={template.name.toLowerCase()}
          popupId={this.createPopupId(key)}
        />
      );
    } else {
      return key;
    }
  }

  generateInvestigationPopup(isObject, key, items) {
    if (isObject) {
      const item = items.find(x => x.itemTemplate.id == key);
      const template = item.itemTemplate;
      return (
        <ItemInvestigationPopup
          key={key}
          item={template}
          popupId={this.createPopupId(key)}
        />
      );
    }
  }

  render() {
    // if (this.state.isLoading) {
    //   return <p>Loading ...</p>;
    // }

    const { roomData } = this.props;
    const { roomTemplate } = roomData;
    const { items } = roomData;

    //  console.log(items);

    //const { room } = this.state.data;

    const generatedDescriptionKeysArray = this.generateDescriptionKeysArray(
      roomTemplate.description
    );

    return (
      <React.Fragment>
        <h2>{roomTemplate.name}</h2>
        <p>
          {generatedDescriptionKeysArray.map(value =>
            this.conditionalLink(value.isObject, value.desc, items)
          )}
        </p>
        <div>
          {generatedDescriptionKeysArray.map(value =>
            this.generateInvestigationPopup(value.isObject, value.desc, items)
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default Room;
