import React, { Component } from "react";

class Item extends Component {
  render() {
    // <!-- Button trigger modal -->
    // <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
    //   Launch demo modal
    // </button>
    return (
      <React.Fragment>
        <a href="#" data-toggle="modal" data-target={"#" + this.props.popupId}>
          {this.props.itemName}
        </a>
      </React.Fragment>
    );
  }
}

export default Item;
