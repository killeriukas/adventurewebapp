import React, { Component } from "react";

class ActionComponent extends Component {
  state = {};

  handleInvestigate(objectId) {
    // console.log("handling investigate. ", this);

    var bodyOject = {
      id: objectId
    };

    fetch("http://localhost:64618/api/action/investigate", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(bodyOject)
    }).then(response => console.log(response.status));
    //  .then(data => console.log(data));
  }

  render() {
    return (
      <React.Fragment>
        <button
          type="button"
          className="btn btn-light"
          //onClick={this.handleInvestigate}
          onClick={() => this.handleInvestigate(1)}
        >
          {/* <button type="button" className="btn btn-light" data-dismiss="modal"> */}
          Investigate
        </button>
        <button type="button" className="btn btn-light">
          Use
        </button>
        <button type="button" className="btn btn-light">
          Pick Up
        </button>
      </React.Fragment>
    );
  }
}

export default ActionComponent;
