import React, { Component } from "react";

class PlayerProfile extends Component {
  state = {
    playerName: "",
    playerEmail: "",
    playerPassword: "none",
    isLoading: false,
    isUpdated: false
  };

  handleNameChange(event) {
    this.setState({ playerName: event.target.value });
  }

  handleEmailChange(event) {
    this.setState({ playerEmail: event.target.value });
  }

  handleCreatePlayer(event) {
    event.preventDefault();
    this.setState({ isLoading: true });

    const bodyObject = {
      name: this.state.playerName,
      email: this.state.playerEmail,
      password: this.state.playerPassword
    };

    fetch("http://localhost:64618/api/playerprofile", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(bodyObject)
    })
      .then(response => response.json())
      .then(data => this.setState({ data, isLoading: false, isUpdated: true }));
  }

  renderPlayerProfileState(playerData) {
    if (playerData.length > 0) {
      const { onSelectedProfile } = this.props;

      return (
        <React.Fragment>
          <h1>Select Your Player</h1>
          <ul>
            {playerData.map(item => (
              <li key={item.id}>
                <button
                  type="button"
                  className="btn btn-light"
                  onClick={() => onSelectedProfile(item.id)}
                >
                  {item.email}
                </button>
              </li>
            ))}
          </ul>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <h1>Create Your Profile</h1>
          <form onSubmit={e => this.handleCreatePlayer(e)}>
            <div className="form-group">
              <label htmlFor="playerName">Name</label>
              <input
                type="text"
                className="form-control"
                id="playerName"
                placeholder="John"
                value={this.state.playerName}
                onChange={e => this.handleNameChange(e)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="playerEmail">Email Address</label>
              <input
                type="email"
                className="form-control"
                id="playerEmail"
                placeholder="Enter email"
                value={this.state.playerEmail}
                onChange={e => this.handleEmailChange(e)}
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </React.Fragment>
      );
    }
  }

  render() {
    if (this.state.isLoading) {
      return <p>loading</p>;
    }

    let { playerData } = this.props;

    if (this.state.isUpdated) {
      const { data } = this.state;

      playerData.push({
        id: data.id,
        name: data.name,
        email: data.email
      });
    }

    return (
      <div className="row">
        <div className="col">{this.renderPlayerProfileState(playerData)}</div>
      </div>
    );
  }
}

export default PlayerProfile;
