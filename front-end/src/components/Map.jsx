import React, { Component } from "react";
import Room from "./Room";

class Map extends Component {
  render() {
    const { mapData } = this.props;
    const { mapTemplate } = mapData;
    const currentRoom = mapData.rooms[mapData.currentRoomIndex];

    return (
      <React.Fragment>
        <h2>{mapTemplate.name + " - " + mapTemplate.description}</h2>
        <Room roomData={currentRoom} />
      </React.Fragment>
    );
  }
}

export default Map;
