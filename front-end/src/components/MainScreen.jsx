import React, { Component } from "react";
import PlayerProfile from "./PlayerProfile";
import Map from "./Map";
import Inventory from "./Inventory";

class MainScreen extends Component {
  state = {
    isLoading: true
  };

  componentDidMount() {
    this.setState({ isLoading: true });

    fetch("http://localhost:64618/api/playerprofile")
      .then(response => response.json())
      .then(data => this.setState({ data, isLoading: false }));
  }

  selectPlayerProfile(data) {
    const { selectedProfile } = this.state;

    if (selectedProfile == null) {
      return (
        <div className="row">
          <div className="col">
            <PlayerProfile
              playerData={data}
              onSelectedProfile={profileId =>
                this.handleProfileSelection(profileId)
              }
            />
          </div>
        </div>
      );
    } else {
      return (
        <React.Fragment>
          <div className="row">
            <div className="col">
              <h1>On Location Adventure</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <p>{"Name: " + selectedProfile.name}</p>
            </div>
            <div className="col-6">
              <p>{"Email: " + selectedProfile.email}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-8">
              <Map mapData={selectedProfile.map} />
            </div>
            <div className="col-4">{/* <Inventory /> */}</div>
          </div>
        </React.Fragment>
      );
    }
  }

  handleProfileSelection(profileId) {
    this.setState({ isLoading: true });

    fetch("http://localhost:64618/api/playerprofile/" + profileId)
      .then(response => response.json())
      .then(profile =>
        this.setState({ selectedProfile: profile, isLoading: false })
      );
  }

  render() {
    if (this.state.isLoading) {
      return <p>loading</p>;
    }

    const { data } = this.state;

    return <div className="container">{this.selectPlayerProfile(data)}</div>;
  }
}

export default MainScreen;
