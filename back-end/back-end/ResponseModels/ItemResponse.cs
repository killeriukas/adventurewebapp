﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.ResponseModels {
    public class ItemResponse {

        public string name { get; set; }

        public string description { get; set; }

        public string descriptionAddendum { get; set; }

        public bool isInvestigated { get; set; }

    }
}
