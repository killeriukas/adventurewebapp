﻿using back_end.Models.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.ResponseModels {
    public class ExampleResponse {

        public MapTemplate mapTemplate { get; set; }

        public RoomTemplate roomTemplate { get; set; }

        public List<ItemTemplate> itemTemplates { get; set; } = new List<ItemTemplate>();

    }
}

