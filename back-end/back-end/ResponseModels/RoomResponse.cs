﻿using back_end.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.ResponseModels {
    public class RoomResponse {

        public Room room { get; set; }

        public Dictionary<int, ItemResponse> itemById = new Dictionary<int, ItemResponse>();

    }
}
