﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Helper {
    public static class CollectionHelper {

        public static void AddNonZero(int id, List<int> list) {
            if(id > 0) {
                list.Add(id);
            }
        }
    }
}
