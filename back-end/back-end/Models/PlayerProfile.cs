﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models {
    public class PlayerProfile {

        [Key]
        public int id { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public Inventory inventory { get; set; }

        public Map map { get; set; }

        //  public Coordinates coordinates { get; set; }



    }
}
