﻿using back_end.Models.Templates;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models {
    public class Room {

        [Key]
        public int id { get; set; }

        public RoomTemplate roomTemplate { get; set; }

        public ICollection<Item> items { get; set; }

        public static async Task<List<Room>> CreateRoomsByMapTemplate(ApplicationDbContext context, MapTemplate mapTemplate) {
            List<Room> rooms = new List<Room>();

            ICollection<int> roomIds = mapTemplate.allRoomIds;

            foreach(int roomId in roomIds) {
                Room room = new Room();
                room.roomTemplate = await context.roomTemplate.FindAsync(roomId);
                room.items = await Item.CreateItemsByRoomTemplate(context, room.roomTemplate);
                rooms.Add(room);
            }

            return rooms;
        }
    }
}
