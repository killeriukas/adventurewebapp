﻿using back_end.Models.Templates;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models
{
    public class Item
    {

        [Key]
        public int id { get; set; }

        public ItemTemplate itemTemplate { get; set; }

        public bool isInvestigated { get; set; }

        public static async Task<ICollection<Item>> CreateItemsByRoomTemplate(ApplicationDbContext context, RoomTemplate roomTemplate) {
            List<Item> items = new List<Item>();

            ICollection<int> itemIds = roomTemplate.itemIds;

            foreach(int itemId in itemIds) {
                Item item = new Item();
                item.itemTemplate = await context.itemTemplate.FindAsync(itemId);
                item.isInvestigated = false;
                items.Add(item);
            }

            return items;
        }
    }
}
