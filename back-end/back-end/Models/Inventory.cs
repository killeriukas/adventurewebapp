﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models {
    public class Inventory {

        [Key]
        public int id { get; set; }

        public ICollection<Item> items { get; set; }

        //public int itemId0 { get; set; }

        //public int itemId1 { get; set; }

        //public int itemId2 { get; set; }

        //public int itemId3 { get; set; }

        //public int itemId4 { get; set; }


    }
}
