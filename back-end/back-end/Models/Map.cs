﻿using back_end.Models.Templates;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models {
    public class Map {

        [Key]
        public int id { get; set; }

        public int currentRoomIndex { get; set; }

        public MapTemplate mapTemplate { get; set; }

        public List<Room> rooms { get; set; }

        public static async Task<Map> CreateMap(ApplicationDbContext context) {
            Map map = new Map();
            map.mapTemplate = await context.mapTemplate.FirstOrDefaultAsync(); //change this to random map later on
            map.rooms = await Room.CreateRoomsByMapTemplate(context, map.mapTemplate);
            map.currentRoomIndex = 0;
            return map;
        }

    }
}
