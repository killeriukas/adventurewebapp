﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models {
    public class Coordinates {

        [Key]
        public int id { get; set; }

        public int currentMapId { get; set; }

        public int currentRoomId { get; set; }

    }
}
