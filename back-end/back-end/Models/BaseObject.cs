﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models
{
    public abstract class BaseObject
    {

        [Key]
        public int id { get; set; }

        [Required]
        [Column(TypeName = "varchar(30)")]
        public string name { get; set; }

        [Required]
        [Column(TypeName = "varchar(160)")]
        public string description { get; set; }

    }
}
