﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.Models.Templates;
using back_end.Models;

namespace back_end.Models {
    public class ApplicationDbContext : DbContext {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {

        }

        public DbSet<PlayerProfile> playerProfile { get; set; }

        public DbSet<RoomTemplate> roomTemplate { get; set; }

        public DbSet<MapTemplate> mapTemplate { get; set; }

        public DbSet<ItemTemplate> itemTemplate { get; set; }

        //public DbSet<Item> itemDetails { get; set; }

        //public DbSet<Room> roomDetails { get; set; }

        //public DbSet<Inventory> inventoryDetails { get; set; }

        //public DbSet<ItemAction> itemActions { get; set; }

        //  public DbSet<Test> testData { get; set; }

    }
}
