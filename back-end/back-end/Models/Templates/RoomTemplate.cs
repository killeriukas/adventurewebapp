﻿using back_end.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models.Templates {

    public class RoomTemplate : BaseObjectTemplate {

        //   [JsonIgnore]
        public int itemId0 { get; set; }

    //    [JsonIgnore]
        public int itemId1 { get; set; }

    //    [JsonIgnore]
        public int itemId2 { get; set; }

    //    [JsonIgnore]
        public int itemId3 { get; set; }

   //     [JsonIgnore]
        public int itemId4 { get; set; }

        [JsonIgnore]
        public ICollection<int> itemIds {
            get {
                List<int> list = new List<int>();
                CollectionHelper.AddNonZero(itemId0, list);
                CollectionHelper.AddNonZero(itemId1, list);
                CollectionHelper.AddNonZero(itemId2, list);
                CollectionHelper.AddNonZero(itemId3, list);
                CollectionHelper.AddNonZero(itemId4, list);
                return list;
            }
        }

    }
}
