﻿using back_end.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models.Templates {
    public class MapTemplate : BaseObjectTemplate {

        //   [JsonIgnore]
        public int roomId0 { get; set; }

        //    [JsonIgnore]
        public int roomId1 { get; set; }

        //    [JsonIgnore]
        public int roomId2 { get; set; }

        //    [JsonIgnore]
        public int roomId3 { get; set; }

        //     [JsonIgnore]
        public int roomId4 { get; set; }


        [JsonIgnore]
        public ICollection<int> allRoomIds {
            get {
                List<int> list = new List<int>();
                CollectionHelper.AddNonZero(roomId0, list);
                CollectionHelper.AddNonZero(roomId1, list);
                CollectionHelper.AddNonZero(roomId2, list);
                CollectionHelper.AddNonZero(roomId3, list);
                CollectionHelper.AddNonZero(roomId4, list);
                return list;
            }
        }
    }
}
