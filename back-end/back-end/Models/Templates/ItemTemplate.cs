﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models.Templates {
    public class ItemTemplate : BaseObjectTemplate {

 //       [Column(TypeName = "varchar(80)")]
        public string descriptionAddendum { get; set; }

   //     [Column(TypeName = "varchar(30)")]
        public string successActionClassName { get; set; }

     //   [Column(TypeName = "varchar(80)")]
        public string taskSuccessDescription { get; set; }

     //   [Column(TypeName = "varchar(80)")]
        public string taskFailedDescription { get; set; }

        public bool isPickable { get; set; }

        public int itemId0 { get; set; }

        public int itemId1 { get; set; }

        public int itemId2 { get; set; }

    }
}
