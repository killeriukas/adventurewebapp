﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Models.Templates {
    public abstract class BaseObjectTemplate {

        [Key]
        public int id { get; set; }

        [Required]
        //[Column(TypeName = "varchar(30)")]
        public string name { get; set; }

        [Required]
        //[Column(TypeName = "varchar(160)")]
        public string description { get; set; }


    }
}
