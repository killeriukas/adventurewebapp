﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.RequestModels {
    public class CreatePlayerProfileRequest {

        public string name { get; set; }

        public string email { get; set; }

        public string password { get; set; }
    }
}
