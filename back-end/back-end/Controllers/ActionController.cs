﻿using back_end.Models;
using back_end.RequestModels;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    public class ActionController : ControllerBase {

        private readonly ApplicationDbContext _context;

        public ActionController(ApplicationDbContext context) {
            _context = context;
        }

        //// GET: api/Action
        //[HttpGet]
        //public IEnumerable<string> Get() {
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/Action/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id) {
        //    return "value";
        //}

        [HttpOptions]
        [Route("[action]")]
        public ActionResult Investigate() {
            return Ok();
        }

        // POST: api/action/investigate
        [HttpPut]
        [Route("[action]")]
        public async Task<ActionResult> Investigate([FromBody] ActionRequest actionRequest) {
            //bool itemExists = _context.itemDetails.Any(item => item.id == actionRequest.id);
            //if(itemExists) {
            //    ItemAction itemAction = await _context.itemActions.FindAsync(actionRequest.id);

            //    //item action doesn't exist, that means it wasn't used yet
            //    if(itemAction == null) {
            //        ItemAction newItemAction = new ItemAction();
            //        newItemAction.isInvestigated = true;

            //        _context.itemActions.Add(newItemAction);
            //        await _context.SaveChangesAsync();

            //        //send the invetigateResponse, which would have a new updated item maybe (if it doesn't already)
            //        //
            //        //return CreatedAtAction();
            //        return Ok();
            //       // return CreatedAtAction("GetItem", new { id = newItemAction.id }, newItemAction);
            //    } else {
            //        //update the record and send the OK or updated item record back
            //        //return Ok();
            //        return NoContent(); //maybe
            //    }
            //} else {
            //    return BadRequest();
            //}
            return BadRequest();
        }

        //// PUT: api/Action/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value) {
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id) {
        //}

    }
}
