﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using back_end.Models;
using back_end.Models.Templates;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomTemplateController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public RoomTemplateController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/RoomTemplate
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoomTemplate>>> GetRoomTemplate()
        {
            return await _context.roomTemplate.ToListAsync();
        }

        //// GET: api/RoomTemplate/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<RoomTemplate>> GetRoomTemplate(int id)
        //{
        //    var roomTemplate = await _context.RoomTemplate.FindAsync(id);

        //    if (roomTemplate == null)
        //    {
        //        return NotFound();
        //    }

        //    return roomTemplate;
        //}

        // PUT: api/RoomTemplate/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutRoomTemplate(int id, RoomTemplate roomTemplate)
        //{
        //    if (id != roomTemplate.id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(roomTemplate).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!RoomTemplateExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/RoomTemplate
        [HttpPost]
        public async Task<ActionResult<RoomTemplate>> PostRoomTemplate(RoomTemplate roomTemplate)
        {
            _context.roomTemplate.Add(roomTemplate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRoomTemplate", new { id = roomTemplate.id }, roomTemplate);
        }

        //// DELETE: api/RoomTemplate/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<RoomTemplate>> DeleteRoomTemplate(int id)
        //{
        //    var roomTemplate = await _context.RoomTemplate.FindAsync(id);
        //    if (roomTemplate == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.RoomTemplate.Remove(roomTemplate);
        //    await _context.SaveChangesAsync();

        //    return roomTemplate;
        //}

        //private bool RoomTemplateExists(int id)
        //{
        //    return _context.RoomTemplate.Any(e => e.id == id);
        //}
    }
}
