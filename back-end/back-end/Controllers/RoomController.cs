﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using back_end.Models;
using back_end.ResponseModels;

namespace back_end.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase {
        private readonly ApplicationDbContext _context;

        public RoomController(ApplicationDbContext context) {
            _context = context;
        }

        //// GET: api/Room
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Room>>> GetroomDetails() {
        //    return await _context.roomDetails.ToListAsync();
        //}

        // GET: api/Room/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<RoomResponse>> GetRoom(int id) {
        //    Room room = await _context.roomDetails.FindAsync(id);

        //    if(room == null) {
        //        return NotFound();
        //    }

        //    RoomResponse roomResponse = new RoomResponse();
        //    roomResponse.room = room;

        //    //List<Task> roomLoadingTasks = new List<Task>();
        //    //roomLoadingTasks.Add(LoadItemForRoom(roomResponse, room.itemId0));
        //    //roomLoadingTasks.Add(LoadItemForRoom(roomResponse, room.itemId1));
        //    //roomLoadingTasks.Add(LoadItemForRoom(roomResponse, room.itemId2));
        //    //roomLoadingTasks.Add(LoadItemForRoom(roomResponse, room.itemId3));
        //    //roomLoadingTasks.Add(LoadItemForRoom(roomResponse, room.itemId4));

        //    //await Task.WhenAll(roomLoadingTasks);

        //    return roomResponse;
        //}

        //private async Task LoadItemForRoom(RoomResponse response, int itemId) {
        //    if(itemId > 0) {
        //        Item item = await _context.itemDetails.FindAsync(itemId);

        //        if(item == null) {
        //            //log some error
        //        } else {
        //            ItemResponse itemResponse = new ItemResponse();
        //            itemResponse.name = item.name;
        //            itemResponse.description = item.description;
        //            itemResponse.descriptionAddendum = item.descriptionAddendum;

        //            //ItemAction itemAction = await _context.itemActions.SingleAsync(q => q.itemId == item.id);

        //            response.itemById.Add(item.id, itemResponse);
        //        }
        //    }
        //}



        //// PUT: api/Room/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutRoom(int id, Room room) {
        //    if(id != room.id) {
        //        return BadRequest();
        //    }

        //    _context.Entry(room).State = EntityState.Modified;

        //    try {
        //        await _context.SaveChangesAsync();
        //    } catch(DbUpdateConcurrencyException) {
        //        if(!RoomExists(id)) {
        //            return NotFound();
        //        } else {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        [HttpPost]
        public async Task<ActionResult<Room>> CreateRoom(Room room) {




            return null;
        }

        //// POST: api/Room
        //[HttpPost]
        //public async Task<ActionResult<Room>> PostRoom(Room room) {
        //    _context.roomDetails.Add(room);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetRoom", new { id = room.id }, room);
        //}

        //// DELETE: api/Room/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Room>> DeleteRoom(int id) {
        //    var room = await _context.roomDetails.FindAsync(id);
        //    if(room == null) {
        //        return NotFound();
        //    }

        //    _context.roomDetails.Remove(room);
        //    await _context.SaveChangesAsync();

        //    return room;
        //}

        //private bool RoomExists(int id) {
        //    return _context.roomDetails.Any(e => e.id == id);
        //}
    }
}
