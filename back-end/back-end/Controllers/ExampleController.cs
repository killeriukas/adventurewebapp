﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using back_end.Models;
using back_end.ResponseModels;
using back_end.Models.Templates;

namespace back_end.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ExampleController : ControllerBase {
        private readonly ApplicationDbContext _context;

        public ExampleController(ApplicationDbContext context) {
            _context = context;
        }

        // GET: api/Example
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ExampleResponse>>> GetExamples() {
            ExampleResponse response = new ExampleResponse();

            MapTemplate mapTemplate = await _context.mapTemplate.FirstOrDefaultAsync();

            //creaate example data
            if(mapTemplate == null) {
                ItemTemplate itemTemplate0 = new ItemTemplate();
                itemTemplate0.name = "Old Drawer";
                itemTemplate0.description = "It's full of dead mice. Gross.";
                itemTemplate0.isPickable = false;
                _context.itemTemplate.Add(itemTemplate0);

                response.itemTemplates.Add(itemTemplate0);

                ItemTemplate itemTemplate1 = new ItemTemplate();
                itemTemplate1.name = "Wooden Cupboard";
                itemTemplate1.itemId0 = 1;
                itemTemplate1.description = "Slightly lit cupboard. The cobwebs are covering everything, but the {" + itemTemplate1.itemId0 + "}.";
                itemTemplate1.isPickable = false;
                _context.itemTemplate.Add(itemTemplate1);

                response.itemTemplates.Add(itemTemplate1);

                ItemTemplate itemTemplate2 = new ItemTemplate();
                itemTemplate2.name = "Granite Doors";
                itemTemplate2.description = "Heavy and bulky doors without a prospect of breaking through. What a lucky day this is!";
                itemTemplate2.isPickable = false;
                _context.itemTemplate.Add(itemTemplate2);

                response.itemTemplates.Add(itemTemplate2);

                RoomTemplate roomTemplate = new RoomTemplate();
                roomTemplate.name = "Bedroom";
                roomTemplate.itemId0 = 2;
                roomTemplate.itemId1 = 1;
                roomTemplate.description = "You wake up in a dirty bed. There is blood everywhere. It's time to vacate the premises. The {"+ roomTemplate.itemId0 + "} are on your left! There is also a {"+ roomTemplate.itemId1 + "} in the far right corner.";
                _context.roomTemplate.Add(roomTemplate);

                response.roomTemplate = roomTemplate;

                mapTemplate = new MapTemplate();
                mapTemplate.name = "Mansion";
                mapTemplate.description = "Dark Mansion";
                mapTemplate.roomId0 = 1;
                _context.mapTemplate.Add(mapTemplate);

                await _context.SaveChangesAsync();
            }

            response.mapTemplate = mapTemplate;

            return new List<ExampleResponse>() { response };
        }
    }
}
