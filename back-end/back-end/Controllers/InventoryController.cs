﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using back_end.Models;
using back_end.ResponseModels;

namespace back_end.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryController : ControllerBase {
        private readonly ApplicationDbContext _context;

        public InventoryController(ApplicationDbContext context) {
            _context = context;
        }

        //// GET: api/Inventory
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Inventory>>> GetinventoryDetails()
        //{
        //    return await _context.inventoryDetails.ToListAsync();
        //}

        // GET: api/Inventory/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InventoryResponse>> GetInventory(int id) {
            //Inventory inventory = await _context.inventoryDetails.FindAsync(id);

            //if(inventory == null) {
            //    return NotFound();
            //}

            //InventoryResponse response = new InventoryResponse();

            //List<Task> tasks = new List<Task>();

            //tasks.Add(LoadItemById(response, inventory.itemId0));
            //tasks.Add(LoadItemById(response, inventory.itemId1));
            //tasks.Add(LoadItemById(response, inventory.itemId2));
            //tasks.Add(LoadItemById(response, inventory.itemId3));
            //tasks.Add(LoadItemById(response, inventory.itemId4));

            //await Task.WhenAll(tasks);

            //return response;
            return BadRequest();
        }

        //private async Task LoadItemById(InventoryResponse response, int id) {
        //    if(id > 0) {
        //     //   Item item = await _context.itemDetails.FindAsync(id);

        //        if(item == null) {
        //            //do something if 
        //        } else {
        //            response.itemList.Add(item);
        //        }
        //    }
        //}

        //// PUT: api/Inventory/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutInventory(int id, Inventory inventory) {
        //    if(id != inventory.id) {
        //        return BadRequest();
        //    }

        //    _context.Entry(inventory).State = EntityState.Modified;

        //    try {
        //        await _context.SaveChangesAsync();
        //    } catch(DbUpdateConcurrencyException) {
        //        if(!InventoryExists(id)) {
        //            return NotFound();
        //        } else {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/Inventory
        //[HttpPost]
        //public async Task<ActionResult<Inventory>> PostInventory(Inventory inventory)
        //{
        //    _context.inventoryDetails.Add(inventory);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetInventory", new { id = inventory.id }, inventory);
        //}

        //// DELETE: api/Inventory/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Inventory>> DeleteInventory(int id)
        //{
        //    var inventory = await _context.inventoryDetails.FindAsync(id);
        //    if (inventory == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.inventoryDetails.Remove(inventory);
        //    await _context.SaveChangesAsync();

        //    return inventory;
        //}

        //private bool InventoryExists(int id) {
        //    return _context.inventoryDetails.Any(e => e.id == id);
        //}
    }
}
