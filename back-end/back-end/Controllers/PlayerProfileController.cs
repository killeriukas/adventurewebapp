﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using back_end.Models;
using back_end.RequestModels;

namespace back_end.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerProfileController : ControllerBase {
        private readonly ApplicationDbContext _context;

        public PlayerProfileController(ApplicationDbContext context) {
            _context = context;
        }

        // GET: api/PlayerProfile
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerProfile>>> GetplayerProfile() {
            return await _context.playerProfile.ToListAsync();
        }

        // GET: api/PlayerProfile/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PlayerProfile>> GetPlayerProfile(int id) {
            PlayerProfile playerProfile = await _context.playerProfile.
                Where(profile => profile.id == id).
                Include(profile => profile.map).
                ThenInclude(map => map.mapTemplate).
                Include(profile => profile.map).
                ThenInclude(map => map.rooms).
                ThenInclude(room => room.roomTemplate).
                Include(profile => profile.map).
                ThenInclude(map => map.rooms).
                ThenInclude(room => room.items).
                ThenInclude(item => item.itemTemplate).
                FirstOrDefaultAsync();
            
            if(playerProfile == null) {
                return NotFound();
            }

            return playerProfile;
        }

        [HttpOptions]
        public ActionResult CreatePlayerProfile() {
            return Ok();
        }

        // POST: api/PlayerProfile
        [HttpPost]
        public async Task<ActionResult<PlayerProfile>> CreatePlayerProfile([FromBody]CreatePlayerProfileRequest playerProfileRequest) {

            PlayerProfile playerProfile = new PlayerProfile();
            playerProfile.name = playerProfileRequest.name;
            playerProfile.email = playerProfileRequest.email;

            playerProfile.map = await Map.CreateMap(_context);

            _context.playerProfile.Add(playerProfile);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlayerProfile", new { id = playerProfile.id }, playerProfile);
        }

    }
}
